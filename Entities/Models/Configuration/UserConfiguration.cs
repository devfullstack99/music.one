﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models.Configuration
{
   

    //public class UserConfiguration : IEntityTypeConfiguration<Employee>
    //{
    //    //public void Configure(EntityTypeBuilder<Employee> builder)
    //    //{
    //    //    builder.HasData(
    //    //        new Employee
    //    //        {
    //    //            Id = new Guid("e310a6cb-6677-4aa6-93c7-2763956f7a97"),
    //    //            Name = "Mark Miens",
    //    //            Age = 26,
    //    //            Position = "Software Developer"
    //    //        },
    //    //        new Employee
    //    //        {
    //    //            Id = new Guid("398d10fe-4b8d-4606-8e9c-bd2c78d4e001"),
    //    //            Name = "Anna Simmons",
    //    //            Age = 29,
    //    //            Position = "Software Developer"
    //    //        }
    //    //    );
    //    //}



    //    public void Configure(EntityTypeBuilder<IdentityRole> builder)
    //    {
    //        builder.HasData(
    //        new IdentityRole
    //        {
    //            Name = "Visitor",
    //            NormalizedName = "VISITOR"
    //        },
    //        new IdentityRole
    //        {
    //            Name = "Administrator",
    //            NormalizedName = "ADMINISTRATOR"
    //        });
    //    }
    //}



    public class RoleConfiguration : IEntityTypeConfiguration<IdentityRole>
    {
        public void Configure(EntityTypeBuilder<IdentityRole> builder)
        {
            builder.HasData(
            new IdentityRole
            {
                Name = "User",
                NormalizedName = "User"
            },
            new IdentityRole
            {
                Name = "Mentor",
                NormalizedName = "MENTOR"
            },
              new IdentityRole
              {
                  Name = "Editor",
                  NormalizedName = "EDITOR"
              },
                new IdentityRole
                {
                    Name = "Admin",
                    NormalizedName = "ADMIN"
                }
            );
        }
    }
}
