﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class InsertRoles : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "9d91543f-2896-438d-a854-1b97aba47987", "3768ef30-7c0c-4e1b-9c94-8cc63e0eb3ad", "User", "User" },
                    { "e17891f6-814b-4268-bd41-33bd190d6734", "0f77d2f2-dfc6-4e0c-a4b1-e6cfd89d0c25", "Mentor", "MENTOR" },
                    { "325edc0f-11f8-4881-8d3f-8440a14bd6d0", "49e76476-0b47-47fe-ab8a-629928be1edb", "Editor", "EDITOR" },
                    { "5c526d3b-e8bb-470a-9961-7bc56dd11518", "94e2be2f-f8e7-45ca-82dd-d00c032139a2", "Admin", "ADMIN" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "325edc0f-11f8-4881-8d3f-8440a14bd6d0");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "5c526d3b-e8bb-470a-9961-7bc56dd11518");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "9d91543f-2896-438d-a854-1b97aba47987");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "e17891f6-814b-4268-bd41-33bd190d6734");
        }
    }
}
