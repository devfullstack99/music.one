﻿

using Entities;

namespace Contracts
{
    public interface IOwnerRepository : IRepositoryBase<Owner>
    {
    }
}