﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MusicOne.Web.API.Common
{
    public class ResponseObject<T>
    {
        private T _data;
        private StatusCodeResult _httpStatus;
        private bool _success = true;
        private string _message;
        private string _errorCode = "0";

        public ResponseObject()
        {
            this._data = default(T);
        }


        public bool success
        {
            get { return _success; }
            set { _success = value; }
        }

        public StatusCodeResult HttpStatus
        {
            get { return _httpStatus; }
            set { _httpStatus = value; }
        }

        public string message
        {
            get { return _message; }
            set { _message = value; }
        }
        public string errorcode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }
        public T data
        {
            get { return _data; }
            set { _data = value; }
        }


    }
}
