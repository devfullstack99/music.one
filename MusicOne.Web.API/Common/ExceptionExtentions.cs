﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MusicOne.Web.API.Common
{
   

    public static class CustomExceptions
    {
        public static string GetAllMessages(this Exception ex)
        {
            if (ex == null)
                throw new ArgumentNullException("ex");

            StringBuilder sb = new StringBuilder();

            while (ex != null)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    if (sb.Length > 0)
                        sb.Append(" ");

                    sb.Append(ex.Message);
                }

                ex = ex.InnerException;
            }

            return sb.ToString();
        }

        public static string GetInnerMessage(this Exception ex)
        {
            if (ex == null)
                throw new ArgumentNullException("ex");

            var message = string.Empty;

            while (ex != null)
            {
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    message = ex.Message;
                }

                ex = ex.InnerException;
            }

            return message;
        }
    }

    public class ExceptionExtension : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="XPressException"/> class.
        /// </summary>
        /// <param name="code">The exception code.</param>
        public ExceptionExtension(int code)
        {
            Code = code;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XPressException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="code">The exception code.</param>
        public ExceptionExtension(string message, int code)
            : base(message)
        {
            Code = code;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="XPressException"/> class.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="innerException">The inner exception.</param>
        /// <param name="code">The exception code.</param>
        public ExceptionExtension(string message, Exception innerException, int code)
            : base(message, innerException)
        {
            Code = code;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BpaBusinessException"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        /// <param name="code">The exception code.</param>
        protected ExceptionExtension(SerializationInfo info, StreamingContext context, int code)
            : base(info, context)
        {
            Code = code;
        }

        public ExceptionExtension(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BpaBusinessException"/> class.
        /// </summary>
        /// <param name="code">The exception code.</param>
        /// <param name="paramsList">The exception message argument list.</param>
        public ExceptionExtension(int code, params object[] paramsList)
            : base(GetExceptionMessage(code, paramsList))
        {
            Code = code;
            this.Parameters = paramsList.ToList();
        }

        public List<object> Parameters { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BpaBusinessException"/> class.
        /// </summary>
        /// <param name="innerException">The inner exception.</param>
        /// <param name="code">The exception code.</param>
        /// <param name="paramsList">The exception message argument list.</param>
        public ExceptionExtension(Exception innerException, int code, params object[] paramsList)
            : base(GetExceptionMessage(code, paramsList), innerException)
        {
            Code = code;
        }

        /// <summary>
        /// Gets the code.
        /// </summary>
        /// <value>The code.</value>
        public int Code { get; private set; }

        /// <summary>
        /// Gets the Exception Message by Exception code.
        /// </summary>
        /// <param name="code">The exception code.</param>
        /// <param name="paramsList">The exception message argument list.</param>

        public static string GetExceptionMessage(int code, params object[] paramsList)
        {
            string format = string.Empty;

            switch (code)
            {
                case 500:
                    format = "Guid Parsing error";
                    break;
                case 550:
                    format = "No dataset mapping defined in orgnisation";
                    break;
                case 1001:
                    format = "User information has expired. Please logout and login again.";
                    break;
                case 1100:
                    format = $"{paramsList[0]} already added as approver. Please add another user.";
                    break;
                case 2001:
                    format = "Unable to activate device status changed.";

                    break;
                case 2002:
                    format = "Unable to find the device details.";
                    break;
                case 2003:
                    format = "Unable to register!e.";
                    break;
                case 2004:
                    format = "Unable to activate device invalid registration code.";
                    break;
                case 2005:
                    format = "Unable to register device user not found.";
                    break;
                case 2006:
                    format = "Unable to register device user not activated.";
                    break;
                case 2007:
                    format = "Device already registered";
                    break;
                case 2008:
                    format = "Invalid Model ";
                    break;

                case 2009:
                    format = "Invalid Code ";
                    break;

                case 2010:
                    format = "Unable to find the Supplier Details in database for update.";
                    break;

                case 2011:
                    format = "Unable to Update Version Changed";
                    break;
                case 2012:
                    format = "Multiple user recorded. Device registration denied.";
                    break;
                case 2013:
                    format = "Same device registered multiple times.";
                    break;

                case 2014:
                    format = "Supplier already exists.";
                    break;
                case 3000: // 3000 - 3500
                    format = "No active workflows found.";
                    break;

                case 2015:
                    format = "Invalid Id.";
                    break;

                default:
                    format = "Unknown exception";
                    break;


            }

            return string.Format(format, paramsList);
        }
    }
}
