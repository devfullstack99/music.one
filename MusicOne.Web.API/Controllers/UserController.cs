﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Entities.DataTransferObjects;
using Entities.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MusicOne.Web.API.Common;

namespace MusicOne.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {

        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;

        public UserController(IMapper mapper, UserManager<User> userManager)
        {
            _mapper = mapper;
            _userManager = userManager;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        /// 
        [HttpPost]
      //  [ValidateAntiForgeryToken]
        [Route("Register")]
        //public async Task<IActionResult<ResponseObject<string>> Register(UserRegistrationModel userModel)
        //{
        public async Task<ResponseObject<string>> Register(UserRegistrationModel userModel)
        {
            ResponseObject<string> responseObject = new ResponseObject<string>()
            { message = "No Data Available", success = false, data = null, HttpStatus = NotFound(), errorcode = "0" };

            try
            {
                if (!ModelState.IsValid)
            {
                throw new Exception("Invalid Model");
            }

            var user = _mapper.Map<User>(userModel);

            var result = await _userManager.CreateAsync(user, userModel.Password);

            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.TryAddModelError(error.Code, error.Description);
                }

               // return View(userModel);
            }
                else
                {
                    await _userManager.AddToRoleAsync(user, "User");
                }
            }

            catch (ExceptionExtension e)
            {
                responseObject = new ResponseObject<string>
                {
                    HttpStatus = Ok(),
                    errorcode = e.Code.ToString(),
                    message = e.Message
                };
            }
            catch (Exception e)
            {
                responseObject = new ResponseObject<string>
                {
                    HttpStatus = Ok(),
                    errorcode = e.Message.ToString(),
                    message = e.Message
                };
            }
            return responseObject;
           

            //return RedirectToAction(nameof(HomeController.Index), "Home");
        }
    }
}
